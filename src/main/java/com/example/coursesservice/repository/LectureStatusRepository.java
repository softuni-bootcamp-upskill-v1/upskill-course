package com.example.coursesservice.repository;

import com.example.coursesservice.model.entity.LectureStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LectureStatusRepository extends JpaRepository<LectureStatusEntity,String> {

    LectureStatusEntity findLectureStatusEntityByEmployeeEmailAndCourseIdAndLectureId(String employeeEmail,
                                                                                      String courseId, String lectureId);

    int countByEmployeeEmailAndCourseIdAndClickedTrue(String employeeEmail, String courseId);
    int countLectureStatusEntitiesByEmployeeEmailAndCourseId(String employeeEmail, String courseId);


    @Query("select distinct lse.employeeEmail from LectureStatusEntity as  lse where lse.courseId=:id")
    List<String> getEmployeeEnrolledCourse(String id);



}
