package com.example.coursesservice.model.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "lecture_status")
public class LectureStatusEntity extends BaseEntity {


    private String employeeEmail;
    private String courseId;
    private String lectureId;
    private boolean isClicked=false;


    public LectureStatusEntity() {
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public LectureStatusEntity setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
        return this;
    }

    public String getCourseId() {
        return courseId;
    }

    public LectureStatusEntity setCourseId(String courseId) {
        this.courseId = courseId;
        return this;
    }

    public String getLectureId() {
        return lectureId;
    }

    public LectureStatusEntity setLectureId(String lectureId) {
        this.lectureId = lectureId;
        return this;
    }

    public boolean isClicked() {
        return isClicked;
    }

    public LectureStatusEntity setClicked(boolean clicked) {
        isClicked = clicked;
        return this;
    }
}
