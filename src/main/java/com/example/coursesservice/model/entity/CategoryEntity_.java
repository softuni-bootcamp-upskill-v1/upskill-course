package com.example.coursesservice.model.entity;

import com.example.coursesservice.model.enums.CategoryNameEnum;

import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import java.util.List;

public abstract class CategoryEntity_ {
    public static volatile SingularAttribute<CategoryEntity, CategoryNameEnum> name;
    public static volatile ListAttribute<CourseEntity,List<CourseEntity>> courses;

    public static final String NAME = "name";
    public static final String COURSES = "courses";
}
