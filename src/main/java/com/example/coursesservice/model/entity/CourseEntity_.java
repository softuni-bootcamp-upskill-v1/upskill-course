package com.example.coursesservice.model.entity;

import com.example.coursesservice.model.enums.StatusNameEnum;

import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@StaticMetamodel(CourseEntity.class)
public abstract class CourseEntity_ {
    public static volatile SingularAttribute<CourseEntity,String> courseId;
    public static volatile SingularAttribute<CourseEntity,String> name;
    public static volatile SingularAttribute<CourseEntity,BigDecimal> price;
    public static volatile SingularAttribute<CourseEntity,StatusNameEnum> status;
    public static volatile SingularAttribute<CourseEntity,String> description;
    public static volatile SingularAttribute<CourseEntity,String> videoUrl;
    public static volatile SingularAttribute<CourseEntity,String> imageUrl;
    public static volatile SingularAttribute<CourseEntity,LocalDateTime> startDate;
    public static volatile SingularAttribute<CourseEntity,LocalDateTime> endDate;
    public static volatile SingularAttribute<CourseEntity,Integer> duration;
    public static volatile ListAttribute<CourseEntity,List<CategoryEntity>> categories;
    public static volatile ListAttribute<CourseEntity,List<LanguageEntity>> languages;
    public static volatile ListAttribute<CourseEntity,List<LectureEntity>> lectures;
    public static volatile SingularAttribute<CourseEntity,String> lector;
    public static volatile SingularAttribute<CourseEntity,String> lectorDescription;
    public static volatile ListAttribute<CourseEntity, List<BusinessOwnerEntity>> businessOwners;
    public static volatile SingularAttribute<CourseEntity,String> skills;
    public static volatile ListAttribute<CourseEntity, List<EmployeeCourseEntity>> employeeCourses;

    public static final String NAME = "name";
    public static final String CATEGORIES = "categories";
    public static final String LANGUAGES = "languages";
    public static final String BUSINESS_OWNERS = "businessOwners";

}
