package com.example.coursesservice.model.entity;

import com.example.coursesservice.model.enums.LanguageEnum;

import javax.persistence.metamodel.SingularAttribute;

public abstract class LanguageEntity_ {
    public static volatile SingularAttribute<LanguageEntity,LanguageEnum> name;

    public static final String NAME = "name";
}
