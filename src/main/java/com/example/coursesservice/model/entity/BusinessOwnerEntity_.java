package com.example.coursesservice.model.entity;

import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import java.util.List;

public class BusinessOwnerEntity_ {
    public static volatile SingularAttribute<BusinessOwnerEntity,String> email;
    public static volatile ListAttribute<BusinessOwnerEntity, List<CourseEntity>> courses;
    public static volatile ListAttribute<BusinessOwnerEntity, List<EmployeeEntity>> employees;

    public static final String EMAIL = "email";
    public static final String COURSES = "courses";
    public static final String EMPLOYEES = "employees";
}
