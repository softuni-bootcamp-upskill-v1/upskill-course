package com.example.coursesservice.model.view;

import java.time.LocalDateTime;

public class CourseAchievementView {
    private String id;
    private String name;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private String grade;

    public CourseAchievementView() {
    }

    public CourseAchievementView(String id, String name, LocalDateTime startDate, LocalDateTime endDate) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getId() {
        return id;
    }

    public CourseAchievementView setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public CourseAchievementView setName(String name) {
        this.name = name;
        return this;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public CourseAchievementView setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
        return this;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public CourseAchievementView setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
        return this;
    }

    public String getGrade() {
        return grade;
    }

    public CourseAchievementView setGrade(String grade) {
        this.grade = grade;
        return this;
    }
}
