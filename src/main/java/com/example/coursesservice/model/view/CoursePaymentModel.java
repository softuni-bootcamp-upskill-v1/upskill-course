package com.example.coursesservice.model.view;

import java.math.BigDecimal;

public class CoursePaymentModel {

    private BigDecimal price;

    public String getCourseId() {
        return courseId;
    }

    public CoursePaymentModel setCourseId(String courseId) {
        this.courseId = courseId;
        return this;
    }

    private String courseId;
    private String name;

    private String businessOwnerId;
    private String email;

    private Integer enrolled;

    public Integer getEnrolled() {
        return enrolled;
    }

    public CoursePaymentModel setEnrolled(Integer enrolled) {
        this.enrolled = enrolled;
        return this;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public CoursePaymentModel setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }


    public String getName() {
        return name;
    }

    public CoursePaymentModel setName(String name) {
        this.name = name;
        return this;
    }

    public String getBusinessOwnerId() {
        return businessOwnerId;
    }

    public CoursePaymentModel setBusinessOwnerId(String businessOwnerId) {
        this.businessOwnerId = businessOwnerId;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public CoursePaymentModel setEmail(String email) {
        this.email = email;
        return this;
    }

    public CoursePaymentModel() {
    }
}
