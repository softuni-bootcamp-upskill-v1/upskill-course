package com.example.coursesservice.model.view;

public class ResponseDto {

    private String message;
    private Boolean success;
    private Object data;

    public ResponseDto(String message, Boolean success, Object data) {
        this.message = message;
        this.success = success;
        this.data = data;
    }

    public ResponseDto() {

    }

    public String getMessage() {
        return message;
    }

    public ResponseDto setMessage(String message) {
        this.message = message;
        return this;
    }

    public Boolean getSuccess() {
        return success;
    }

    public ResponseDto setSuccess(Boolean success) {
        this.success = success;
        return this;
    }

    public Object getData() {
        return data;
    }

    public ResponseDto setData(Object data) {
        this.data = data;
        return this;
    }
}
