package com.example.coursesservice.model.view;

import java.io.Serializable;

public class CourseStatusChange implements Serializable {

    String changedStatus;
    String courseId;
    String email;

    public CourseStatusChange() {
    }

    public CourseStatusChange(String changedStatus, String courseId, String email) {
        this.changedStatus = changedStatus;
        this.courseId = courseId;
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public CourseStatusChange setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getCourseId() {
        return courseId;
    }

    public CourseStatusChange setCourseId(String courseId) {
        this.courseId = courseId;
        return this;
    }

    public String getChangedStatus() {
        return changedStatus;
    }

    public CourseStatusChange setChangedStatus(String changedStatus) {
        this.changedStatus = changedStatus;
        return this;
    }
}
