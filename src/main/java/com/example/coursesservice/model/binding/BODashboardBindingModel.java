package com.example.coursesservice.model.binding;

public class BODashboardBindingModel {

    private Integer coursesCount;

    public BODashboardBindingModel() {
    }

    public Integer getCoursesCount() {
        return coursesCount;
    }

    public BODashboardBindingModel setCoursesCount(Integer coursesCount) {
        this.coursesCount = coursesCount;
        return this;
    }
}
