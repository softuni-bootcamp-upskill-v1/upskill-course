package com.example.coursesservice.model.specification;
import com.example.coursesservice.model.entity.*;
import com.example.coursesservice.model.enums.CategoryNameEnum;
import com.example.coursesservice.model.enums.LanguageEnum;
import org.springframework.data.jpa.domain.Specification;
import javax.persistence.criteria.Join;
import java.util.List;


public class CourseSpecifications {


    public static Specification<CourseEntity> searchByCategory(List<CategoryNameEnum> categoryNameEnumList) {
        return (root, query, cb) -> {
            Join<CategoryEntity, CourseEntity> join = root.join(CourseEntity_.CATEGORIES);
            query.distinct(true);
            return cb.in(join.get(CategoryEntity_.NAME)).value(categoryNameEnumList);
        };
    }

    public static Specification<CourseEntity> searchByLanguage(List<LanguageEnum> languageNameEnumList) {

        return (root, query, cb) -> {
            Join<LanguageEntity, CourseEntity> join = root.join(CourseEntity_.LANGUAGES);
            query.distinct(true);
            return cb.in(join.get(LanguageEntity_.NAME)).value(languageNameEnumList);
        };
    }

    public static Specification<CourseEntity> notInBoCourses(String boEmail) {
        return (root, query, cb) -> {
            Join<BusinessOwnerEntity, CourseEntity> join = root.join(CourseEntity_.BUSINESS_OWNERS);
            return cb.equal(join.get(BusinessOwnerEntity_.EMAIL), boEmail).not();
        };
    }




}
