package com.example.coursesservice.exception;

public class DeleteEnrolledCourseException extends RuntimeException{

    public DeleteEnrolledCourseException() {
        super("The course is enrolled");
    }
}
