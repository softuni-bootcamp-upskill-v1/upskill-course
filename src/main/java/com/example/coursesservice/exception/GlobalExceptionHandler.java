package com.example.coursesservice.exception;

import com.example.coursesservice.model.view.ResponseDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.persistence.EntityNotFoundException;
import java.util.NoSuchElementException;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = CourseNotFoundException.class)
    public ResponseEntity<?> handleCourseNotFoundException(CourseNotFoundException exception){

        ResponseDto responseDto = new ResponseDto()
                .setMessage(exception.getMessage())
                .setData(exception)
                .setSuccess(Boolean.FALSE);

        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(responseDto);
    }

    @ExceptionHandler(value = EntityNotFoundException.class)
    public ResponseEntity<?> handleEntityNotFoundException(EntityNotFoundException exception){

        ResponseDto responseDto = new ResponseDto()
                .setMessage("Entity with the given ID does not exist!")
                .setData(exception)
                .setSuccess(Boolean.FALSE);

        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(responseDto);
    }

    @ExceptionHandler(value = DeleteEnrolledCourseException.class)
    public ResponseEntity<?> handleCDeleteEnrolledCourseException(DeleteEnrolledCourseException exception){

        ResponseDto responseDto = new ResponseDto()
                .setMessage(exception.getMessage())
                .setData(exception)
                .setSuccess(Boolean.FALSE);

        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(responseDto);
    }

    @ExceptionHandler(value = CourseDuplicationException.class)
    public ResponseEntity<?> handleCourseDuplicationException(CourseDuplicationException exception){

        ResponseDto responseDto = new ResponseDto()
                .setMessage(exception.getMessage())
                .setData(exception)
                .setSuccess(Boolean.FALSE);

        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(responseDto);
    }

    @ExceptionHandler(value = {NullPointerException.class, NoSuchElementException.class})
    public ResponseEntity<?> handleNullPointerException(Exception exception){

        ResponseDto responseDto = new ResponseDto()
                .setMessage("Opss something goes wrong.")
                .setData(exception)
                .setSuccess(Boolean.FALSE);

        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(responseDto);
    }

    @ExceptionHandler(value = BusinessOwnerNotFoundException.class)
    public ResponseEntity<?> handleBusinessOwnerNotFoundException(BusinessOwnerNotFoundException exception){

        ResponseDto responseDto = new ResponseDto()
                .setMessage(exception.getMessage())
                .setData(exception)
                .setSuccess(Boolean.FALSE);

        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(responseDto);
    }

}
