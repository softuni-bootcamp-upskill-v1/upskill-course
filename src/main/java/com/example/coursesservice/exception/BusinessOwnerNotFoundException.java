package com.example.coursesservice.exception;

public class BusinessOwnerNotFoundException extends RuntimeException{

    private String email;

    public BusinessOwnerNotFoundException(String email) {
        super("Business owner with email" + email + " does not exists!");
        this.email = email;
    }

    public String getEmail() {
        return email;
    }
}
