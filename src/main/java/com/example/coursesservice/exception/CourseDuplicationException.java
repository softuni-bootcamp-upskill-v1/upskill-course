package com.example.coursesservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class CourseDuplicationException extends RuntimeException {

    private String name;

    public CourseDuplicationException(String name) {
        super("Course with name " + name + " already exist");
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
