package com.example.coursesservice.service;

import com.example.coursesservice.model.view.CourseAchievementView;
import org.springframework.stereotype.Service;

import java.util.List;


public interface LectureStatusService {
    List<CourseAchievementView> getCoursesAchievement(String email);
}
