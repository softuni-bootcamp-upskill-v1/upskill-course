package com.example.coursesservice.service;

import com.example.coursesservice.model.binding.BODashboardBindingModel;
import com.example.coursesservice.model.entity.BusinessOwnerEntity;
import com.example.coursesservice.model.view.CourseNameEnrolledView;
import com.example.coursesservice.model.view.EmployeesByCompanyOwnerView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface BusinessOwnerService {

    BusinessOwnerEntity getBusinessOwnerByEmail(String companyOwnerEmail);

    boolean isBusinessOwnerExist(String businessOwnerEmail);

    void createBusinessOwner(String businessOwnerEmail);

    void save(BusinessOwnerEntity businessOwner);

    void addEmployeesToBusinessOwner(List<EmployeesByCompanyOwnerView> employeesByCompanyOwnerViews);

    BODashboardBindingModel getBusinessOwnerCoursesCount(HttpServletRequest request);
}
