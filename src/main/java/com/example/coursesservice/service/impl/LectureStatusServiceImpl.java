package com.example.coursesservice.service.impl;

import com.example.coursesservice.model.view.CourseAchievementView;
import com.example.coursesservice.repository.CourseRepository;
import com.example.coursesservice.repository.LectureStatusRepository;
import com.example.coursesservice.service.LectureStatusService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LectureStatusServiceImpl implements LectureStatusService {
    private final LectureStatusRepository lectureStatusRepository;
    private final CourseRepository courseRepository;

    public LectureStatusServiceImpl(LectureStatusRepository lectureStatusRepository, CourseRepository courseRepository) {
        this.lectureStatusRepository = lectureStatusRepository;
        this.courseRepository = courseRepository;
    }

    @Override
    public List<CourseAchievementView> getCoursesAchievement(String email) {
        List<CourseAchievementView> allCoursesByEmployee = this.courseRepository.getCoursesAchievementByEmployee(email);


        allCoursesByEmployee.forEach(course->{

            int totalLectures = this.lectureStatusRepository.countLectureStatusEntitiesByEmployeeEmailAndCourseId(email,course.getId());
            int clickedLectures = this.lectureStatusRepository.countByEmployeeEmailAndCourseIdAndClickedTrue(email, course.getId());

            String grade = "Pending";

            if(clickedLectures!=0){
                grade= String.format("%.0f/100",clickedLectures*1.00/totalLectures*100);
            }course.setGrade(grade);
        });

        return allCoursesByEmployee;
    }
}
