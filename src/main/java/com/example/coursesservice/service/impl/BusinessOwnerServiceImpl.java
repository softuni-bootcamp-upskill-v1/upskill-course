package com.example.coursesservice.service.impl;

import com.example.coursesservice.model.binding.BODashboardBindingModel;
import com.example.coursesservice.model.entity.BusinessOwnerEntity;
import com.example.coursesservice.model.entity.CourseEntity;
import com.example.coursesservice.model.view.CourseNameEnrolledView;
import com.example.coursesservice.model.view.EmployeesByCompanyOwnerView;
import com.example.coursesservice.repository.BusinessOwnerRepository;
import com.example.coursesservice.repository.CourseRepository;
import com.example.coursesservice.service.BusinessOwnerService;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
public class BusinessOwnerServiceImpl implements BusinessOwnerService {

    private final BusinessOwnerRepository businessOwnerRepository;
    private final CourseRepository courseRepository;

    public BusinessOwnerServiceImpl(BusinessOwnerRepository businessOwnerRepository, CourseRepository courseRepository) {
        this.businessOwnerRepository = businessOwnerRepository;
        this.courseRepository = courseRepository;
    }

    @Override
    public BusinessOwnerEntity getBusinessOwnerByEmail(String companyOwnerEmail) {
        return this.businessOwnerRepository.getBusinessOwnerEntityByEmail(companyOwnerEmail);
    }

    @Override
    public boolean isBusinessOwnerExist(String businessOwnerEmail) {

        return this.businessOwnerRepository.existsByEmail(businessOwnerEmail);
    }

    @Override
    public void createBusinessOwner(String businessOwnerEmail) {

        BusinessOwnerEntity businessOwnerEntity = new BusinessOwnerEntity()
                .setEmail(businessOwnerEmail)
                .setCourses(new ArrayList<>());

        this.businessOwnerRepository.save(businessOwnerEntity);
    }

    @Override
    public void save(BusinessOwnerEntity businessOwner) {
        this.businessOwnerRepository.save(businessOwner);
    }

    @Override
    public void addEmployeesToBusinessOwner(List<EmployeesByCompanyOwnerView> employeesByCompanyOwnerViews) {


    }

    @Override
    public BODashboardBindingModel getBusinessOwnerCoursesCount(HttpServletRequest request) {

        String businessOwnerEmail = request.getHeader("X-User-Email");
        BusinessOwnerEntity businessOwnerEntityByEmail = this.businessOwnerRepository.getBusinessOwnerEntityByEmail(businessOwnerEmail);

        return new BODashboardBindingModel().setCoursesCount(businessOwnerEntityByEmail.getCourses().size());
    }
}
